<?php


    if ( isset($_POST['requisicao']) ) {


        include '../database/conexao.php';
        session_start();
    
        switch ( $_POST['requisicao'] ) {
    
            case "enviarMensagem":
    
                $sql = "INSERT INTO mensagem (usuario, mensagem) VALUES (?, ?)";
                $stmt = $conexao->prepare($sql);
                
                $stmt->bindParam(1, $_SESSION['usuario']);
                $stmt->bindParam(2, $_REQUEST['mensagem']);

                $stmt->execute();

                if ($stmt) {
                    echo 1;
                    exit;
                }
    
            break;

            case "carregarMensagens":

                $sql = "SELECT * FROM mensagem";
                $stmt = $conexao->prepare($sql);

                $stmt->execute();

                
                $mensagens = '';
                while ($mensagem = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    if ($mensagem['usuario'] == $_SESSION['usuario'])
                        $mensagens .= '<div class="mensagem msg-rec">';
                    else
                        $mensagens .= '<div class="mensagem msg-outro float-right">';
                    $mensagens .= '    <div class="container">';
                    $mensagens .= '        <div class="row">';
                    $mensagens .= '            <div class="col">';
                    $mensagens .= '                <div class="msg-usuario font-small font-weight-bold">'.$mensagem['usuario'].'</div>';
                    $mensagens .= '            </div>';
                    $mensagens .= '            <div class="col">';
                    $mensagens .= '                <div class="msg-data font-small font-weight-bold">'.date("d/m/Y H:i", strtotime($mensagem['data'])).'</div>';
                    $mensagens .= '            </div>';
                    $mensagens .= '        </div>';
                    $mensagens .= '        <div class="msg-texto">'.$mensagem['mensagem'].'</div>';
                    $mensagens .= '    </div>';
                    $mensagens .= '</div>';
                    $mensagens .= '<div class="clearfix"></div>';
                }
                
                print_r($mensagens);

            break;
    
        }



    }
