<?php

    $host       = "localhost";
    $usuario    = "root";
    $banco      = "chat";
    $senha      = "";

    try {
        $conexao = new PDO("mysql:dbhost=$host;dbname=$banco", $usuario, $senha);
    } catch ( PDOException $e ) {
        echo $e->getMessage();
    }