<?php
    session_start();
    $_SESSION['usuario'] = "pietroz";
?>
<!DOCTYPE html>
<html>
    
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Sistema de Chat</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Ícone -->
        <link rel="shortcut icon" href="logo.png" type="image/x-icon">

        <!-- CSS -->
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/mdb.css">
        <link rel="stylesheet" href="css/chat.css">
        <link rel="stylesheet" href="css/fontes.css">
        
    </head>
    
    <body>


        <main class="wrapper">
            
            <div class="container">

                <h1>Sistema de Chat</h1>

                <section class="chat_wrapper">
                
                    <div id="chat" class="p-2">
                        <!-- Preenchido com AJAX -->
                    </div>
                
                    <form id="form-mensagem" method="post">
                        <textarea class="form-control" name="mensagem" id="caixa-mensagem" cols="30" rows="4"></textarea>
                    </form>
                
                </section>
                
            </div>
        
        
        </main>



        
        <!-- JavaScript -->
        <script src="js/popper.min.js"></script>
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/mdb.js"></script>

        <script>

            $(document).ready(function() {

                carregaChat();

            });

            // Recarrega o chat a cada 1 segundo
            setInterval(function() {
                carregaChat();
            }, 1000);

            function carregaChat() {

                $.ajax({
                    url: 'scripts/mensagens.php',
                    method: 'post',
                    data: {
                        requisicao: "carregarMensagens"
                    },
                    success: function(retorno) {

                        
                        var scrollPos = $('#chat').scrollTop();
                        var scrollPos = parseInt(scrollPos) + 520;
                        var scrollHeight = $('#chat').prop('scrollHeight');
                        
                        // Coloca as mensagens no chat
                        $('#chat').html(retorno);

                        if (scrollPos >= scrollHeight)
                            $('#chat').scrollTop( $('#chat').prop('scrollHeight') );


                    },
                    error: function(retorno) {
                        alert('Erro');
                        console.log(retorno);
                    }
                });

            }
        
            $('#caixa-mensagem').keyup(function(e) {

                // Quando o usuário digitar 'ENTER', fazer submit
                if ( e.which == 13 ) {
                    $('#form-mensagem').submit();
                }

            });


            $('#form-mensagem').submit(function(e) {

                // Recupera a mensagem
                var mensagem = $('#caixa-mensagem').val();

                $.ajax({
                    url: 'scripts/mensagens.php',
                    method: 'post',
                    data: {
                        requisicao: "enviarMensagem",
                        mensagem: mensagem
                    },
                    success: function(retorno) {
                        
                        if (retorno == 1) {
                            document.getElementById('form-mensagem').reset();
                            carregaChat();
                        }


                    },
                    error: function(retorno) {
                        alert('Erro');
                        console.log(retorno);
                    }
                });

                return false;

            });

        
        </script>

    </body>

</html>